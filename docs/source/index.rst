.. RTD-Sphinx documentation master file, created by
   sphinx-quickstart on Fri Jul  5 12:15:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ReportMiner 7's documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :caption: User Guide
   :name: user-guide-toc
   
   user-guide/reportminer-tutorial.md
   user-guide/creating-a-report-model.md
   user-guide/using-single-and-multicolumn-regions.md
   user-guide/auto-creating-data-regions-and-fields.md
   user-guide/report-options.md
   user-guide/data-fields.md
   user-guide/how-to-use-email-source-object-in-a-report-model.md
   user-guide/importing-monarch-models.md
   user-guide/microsoft-word-and-rich-text-format-support.md
   user-guide/optical-character-recognition-support.md
   user-guide/pdf-scaling-factor.md
   user-guide/using-floating-fields.md
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
