# Data Fields

**Applies To:** 

- ReportMiner Enterprise and Express (all versions) 
- Centerprise Enterprise and Express (all versions) 

## Properties 

To configure the properties of a data field, right-click it and select Properties from the context menu. The following properties are available:  

![](data-fields-images/1.png)

**Name:** Enter the name of your field here.   This name will identify the field on the layout, and it could also be used in any expressions or formulas. Names must be unique from one another and can not have spaces. 

**Data Type:** This sets which kind of data the software looks for and reads. Most often it’s String data, but this option can also be set to Boolean, date, integer, or real. 

**Composite type:** If your field contains composite data, you can specify what kind of composite type the data is. Composite data are details about a record that can be broken down into smaller chunks. For example, a record about a person might contain an address. Address fields are processed by the built-in parser that splits the address string into several smaller strings, such as: street name, city, state, etc. 

 ![](data-fields-images/2.png)

The "Format" menu applies to the "Date" data type. One of the following date formats can be selected where: 

•dd = the day 

•mm, MMM, MM = the month 

•yy, yyyy = the year 

 

**Value if null:** If the field has no value in the report, these are your options for what the program will export in its place should you choose to do so. Note: “null” as used here refers to either an absence of data or an undefined value. Space characters are read as just that – characters. 

*•None:* This is the default setting. If “None” is selected, the field will remain the same. For example, if the field in question is an empty address field, the cell will be empty when the dataset is previewed. 

*•Apply specified default:* A string can be typed in for use here, such as “N/A.” When the program found a null value, “N/A” would appear in the previewed cell instead of it being empty. 

*•Use from previous record:* This applies the value from the same field of the previous record. For instance, if there were a field made up of letters such as: 
 



| A    |
| ---- |
| B    |
|      |
| C    |

 
 
In the above example, the cell below "B" is read as a null value. Should the user select, “Use from previous record", the previewed data would appear as such: 
 
 

| A    |
| ---- |
| B    |
| B    |
| C    |

 
The “B” above the empty value was substituted into the empty value’s cell. 

 

**Remove Extra Spaces Inside Text:** This option removes extra spaces inside the source document’s text. This is most helpful for cases like fixed length files, where there may be many extra spaces between characters. Removing the extra spaces makes the data simpler to read and to work with once exported. If your data contains extra spaces (two or more in a row) and you wish to export it as such, do not check this box. Multiple spaces will be condensed into a single space. 

 

**Remove Text Qualifier (Surrounding quotes):** This option removes quotation marks from your data. This is especially useful for CSV files with many quotation marks as it converts them into an easier-to-manipulate format. Without the option to remove surrounding quotes from a CSV file, the program would include those quotes in the data extracted. For example, “’John Jacob’” “’Jingleheimer Schmidt’” would become “John Jacob Jingleheimer Schmidt.” 

 

**Start position:** The Start Position menu allows you to manually set where your data fields begin.  

 ![](data-fields-images/3.png)

**Line/Column:** There is an invisible grid that overlays every report model. Much like a map, these have values of latitude (column) and longitude (line) that can be used to reference a single point on a data field. The flashing bar that appears when you match patterns or click within your data field is the visual representation of where you are in the Report Model. You can use these coordinates to tell the software which point your start position begins at. The values for “Line:” and “Column:” can be found in the bottom-right corner of your report model when a point on the model is selected. 

 ![](data-fields-images/4.png)

**After String (X) in Previous Line:** This will set the field to start after a specified string in the previous line of data. For example, if a file with medical records began every set of patient data with “Patient Information” above it, the program would set the field to begin the line after “Patient Information” so that all relevant information to each patient record was captured. 

 

**After String (X) in Current Line:** Similar to the explanation above, this will set the field to start after a specified string in the same line as the data field. 

 ![](data-fields-images/length.png)

**Length** is where you can set the length of your data field. The following options are available: 

*•Characters (#):* This lets you set the length of your data field to be a certain number of characters long. 

*•Ends in 2 consecutive blanks*

*•Till the End of the Line:* This ends your data field after the last character in the line. 

*•Till string (X):* This ends your data once it reaches a specified string, such as, “Report End.” 

 ![](data-fields-images/height_info.png)

**Height** lets you set the height of your data field. The following options are available: 

*•Lines (#):* This lets you set the length of your data field to be a certain number of characters high. 

*•Till Region Ends:* The data field will continue until the data region ends. The default setting of data fields, especially ones what vary in height, is to measure them by line count, or how many lines tall they are. This determines the height of a data field based on how long the data region is. 

*•Till Blank Row:* The field will end once a blank row is reached. 

*•Ends at Row with Blank First Character:* Your field will end once it reaches a row with a blank first character (a space.) 

*•Ends at Row with Blank Last Character:* Your field will end once it reaches a row with a blank last character (a space.) 

 

## Properties Options in the Toolbar 

 ![](data-fields-images/5.png)

There are buttons in the toolbar that modify data fields. They are as follows: 

![](data-fields-images/moveleftright.png)  **Move field marker left/right one character:** These buttons will shift your data field to the right or the left one character. 

![](data-fields-images/addsubtract.png) **Decrease/Increase field length by one character:** This expands or shrinks the area that the data field encompasses by one character. 

![](data-fields-images/autodetermine.png) **Auto determine field length:** This determines the field length for you. 

![](data-fields-images/properties.png) **Field Properties…:** This is another way to access the field properties. 

![](data-fields-images/delete.png) **Delete Field:** This deletes your field. 

## Example  

Highlight the area within your data region that you wish to capture as a data field. Right click and select “Add Data Field…” 

 ![](data-fields-images/6.png)

Your fields should switch to another color (the default mode makes them navy.) 

 ![](data-fields-images/7.png)

Be sure to name your data fields — it can get confusing otherwise. This is the view of the Report Model as shown in the Model Layout Window. The data fields in the left-hand box aren’t named: FIELD_# is the default name. The right-hand box’s fields are easier to identify. 

| ![](data-fields-images/modellayout8b.png) |
| ------------------------------------ | ---------------------------------------------------------- |
|                                      |                                                            |

Your data fields are now ready to preview. 

 ![](data-fields-images/9.png)