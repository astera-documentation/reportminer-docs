# Importing Monarch Models

**Applies To:** 

- Centerprise Express/Enterprise  
- ReportMiner Express/Enterprise   

## Overview

If you are a former Monarch user, you have the ability to use your Monarch models within Centerprise/ReportMiner. You can load and convert your old Monarch files by opening up your Monarch xmods in ReportMiner. These models will automatically be converted to a usable report model. With the built in converter, the Monarch model logic is converted to Report model logic.  

### What's the difference between Monarch models and Report Models? 

The main difference between Monarch models and Report models are the type of data region used. Monarch models are all appended data. In ReportMiner the models consists of a hierarchy with a top or parent region and child regions. It is a tree structure. This better represents how the data is extracted.  

### Best Practices

- When converting your xmod, always make sure to preview your model to make sure everything is being extracted correctly. 
- If your Monarch model is using a PDF, you may need to adjust the scaling factor.

## Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/uTtXxVDqNxI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example

Open ReportMiner and click the open file icon  ![](importing-monarch-models-images/3dc2fb8b81131014846f248439925dbd29a6d3426b070eb382cb2019b9b616e7.png)  

A window will appear, enabling you to select your monarch model. Select your xmod file and click Open. 

![](importing-monarch-models-images/e2b7a8ae87982c2eb8f6312bf4267cdec91a68f73578089e2ce8ec23903b251f.png) 

Once you do this, the report options window will appear. Here is where you will select the file from which you are extracting your data.  

 

![](importing-monarch-models-images/1a9d083316a96a08710a1937eaee2ccbb5669b88ad9b49d8a6c568ca411bc4b8.png) 

  

Once you select your file, ReportMiner will convert the extension from xmod to rmd. 

 ![](importing-monarch-models-images/65b0523240c2e8cf2adcbfe24eaf9201c7d3791a40f6935446229530286e27d2.png) 

 Now you have a usable report model. You can adjust this model or proceed to export settings.  