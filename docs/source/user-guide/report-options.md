# Report Options

**Applies To:**  

- Centerprise Express/Enterprise   
- ReportMiner Express/Enterprise   

## Overview: 

Report options are where you can specify or adjust the settings for the report file used for your model. The report options window appears when you are creating a new model and selecting the report you will be extracting from.  

![](report-options-images/34748000131a72286588592321eac22f510596bd73b0a54903edc1eaa699a61d.png) 

You can also open this window by clicking on the "Edit Report Options" icon > ![](report-options-images/a6980a8d06b3ed3cb2fa2b9c16f66f9e4fd3b08a1fd7c58eaf89374ddc65e8d3.png) 

The Report options consists of these components: 

- Data File Path 
- Loop Through All the Files in a Folder (Enterprise edition only) 
- Reading Options 
- Other Options 



### Data File Path 

![](report-options-images/c56a288308e1c667f26cfcb70ea20c397b631a62608415541baf8eab2795983d.png) 

This is where you will select the file you would like to extract from. Click the folder icon > ![](report-options-images/879d8f5f83a9ce3c177c7874a0ced2c2305d6b2922e511c6788a4cd9f748be26.png)and select the file you will be using for your report model. 

### Loop Through All the Files in a Folder (Enterprise Edition Only)

![](report-options-images/ef593397c03e3456d2cff5a804ad92710d38fbc77bd2fda1f635a0797f25fb19.png) 

This option is for when you would like to apply your report model to multiple files in a folder. This is only available in the Enterprise edition and will be greyed out if you are using the express edition. (See Looping Through a Folder Doc). 

 

## Reading Options

This is where you can edit how the file is read. Reading Options can change depending on your file type. ReportMiner currently supports: TXT, PRN, CSV, PDF, EXCEL, and COBOL file types. 

### PDF Reading Options 

![](report-options-images/0fa4a85292b89b98ac13cd7ec3861908b94443bba365a5948fb0552673deab40.png) 

**Run OCR** **(Optical Character Reader):** This option is for image based pdfs. When this box is selected, your image will be scanned and converted into text. (See OCR).  

**Maintain Original Layout:** By default, this option is selected but if you do not want to maintain the original layout, you can un-check this box. 

**Remove Blank Lines**: If selected, this option will remove all blank lines within your report file. 

**Scaling Factor**: The Scaling Factor applies to your pdf's spacing value. In some cases, a pdf writer causes the scaling to be off when converting to ReportMiner text. If this is the case, you can fix this by changing the scaling factor value. (see PDF Scaling Factor Doc). 

**Owner and User Password**: This option is for password protected PDFs. This is where you would insert the credentials.  

 

## Excel Reading Options: 

 

![](report-options-images/15e40184e5eb494d22ef8b01e894443dfdd277a6447b4df3989387773c1742e7.png) 

**Worksheet:** Here you can select the worksheet you would like to extract data from. 

**Space Between Columns**: This option allows you to increase or decrease the spacing between the excel columns to allow a clearer visual.  

 

## Other Options:  

![](report-options-images/0533250644417e1b5491d3f18a50e2d8e2b68cc319d36ced1a32d031008796ab.png) 

Other options is located at the bottom of the Report Options window in blue text. To expand these options click the plus button. Once you do this you will see more options below: 

 

**Tab size**: You can increase or decrease the tab size for your report. The default value is 8 characters. 

**Line Count**: This controls how many lines of the report will be loaded in the report editor. This setting does not affect the amount of data exported. The default value is 5000.  

**Font**: Here is where you can change the font of the text that appears within your report. You have the option to select the font size and bold or italicize the text.  

**Encoding**: You can choose the Encoding for your file. The default is Unicode (UTF-8) but you can change this if you have a file with specific characters. 

**Display Style**: Here you can select the theme for the ReportMiner editor. Choose which color scheme you prefer.

**Record Delimiter:** This option allows you to select the record separator or line break type. You can select \<CR>\<LF>, \<CR>, or \<LF>. 

- \<CR> (Carriage Return) moves the cursor to the beginning of the line without advancing to the next line. 
- \<LF> (Line Feed) moves the cursor down to the next line without returning to the beginning of the line. 
- \<CR>\<LF> does both. 

**Numeric Format:** This is where you can specify the number of decimal digits.  