# Auto Creating Data Regions and Fields

**Applies To:**

- Centerprise  
- ReportMiner

## Overview

In order to export data from report models, data regions and data fields must be created. One way to do this is through the “Auto Create (Fields or Regions)” option. This method takes two lines as a sample of the data that is to be extracted, and creates the desired region or fields with them. This is the simplest, fastest method for creating data regions and fields. 

## Video

<iframe width="1261" height="709" src="https://www.youtube.com/embed/JUYS6kRflCI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Creating Your Region


![](auto-creating-data-regions-and-fields-images/438859d52815596a07fbc293ff4f7c82009f31235e0126f3f3cb92ec4bc6f66f.png) 

To auto create a data region, click on the grey column to the left of one of the lines that is to be included in your region. 

![](auto-creating-data-regions-and-fields-images/c380b9030568f866062df9d46af11e18356ba643c4bbd5d2823947e3df9dd63a.png) 

A green marker will appear: this means that the line is to be included. Clicking on the green marker again will make it turn red, which excludes the line. Clicking a third time will delete the marker.  

Using this method, specify two lines that you wish to capture. Once two lines are indicated, the software creates a data region based on the selections. Note the orange bar (the pattern matching bar) at the top of the window below: the software has used a “match any alphabet” character above where the auction item numbers begin. This is because each auction item number begins with a letter, but not all of them are the same letter. 

![](auto-creating-data-regions-and-fields-images/ffc0e3204773348a31ae1b8dbe501eb8c1dc059afb6dacac70c150ba69935161.png) 

Your data region has been created. Data that can be captured will be highlighted, and a data region node (which has a name default of “Data”) will appear underneath your Record Node in the Model Layout display to the left of your report model. 

![](auto-creating-data-regions-and-fields-images/89144cf1f1e2bf800ff956c4c00559d5ab4ef97c11c7122db70f7a19bdf602aa.png) 

 

## Renaming Data Regions: 

Data regions can be named by editing the “Name” field in the Region Properties menu above the report model editor. Astera recommends naming each data region and field that is created, as this avoids confusion. 

![](auto-creating-data-regions-and-fields-images/09f1a46fda4a6393e284f708d1f5be7efd669dd6720dcb8aa774d5538546baf4.png) 

Your data region is now complete.  

## Creating Your Fields: 

Once your data region has been selected, the next step to a complete report model is to create data fields: this shows exactly what data will be extracted from your report. As with the data region, the software can also automatically create your data fields. Right-click on your data region and select, “Auto Create Fields.” 

![](auto-creating-data-regions-and-fields-images/e5774410a266adc4d99003f37c67a1b0b27cdff266f2f98dd2a84202b170e63a.png) 

Your data fields have now been created. Specifically, in figure one, you can see that there have been two data fields created – one for the item numbers and one for the item descriptions. The left field has been clicked on, which is why it appears as navy with light text whereas the other field is a light blue with dark text. 

![](auto-creating-data-regions-and-fields-images/d8cb7f96088aacb0b247fb62fdb6a7c3f853e5c35b8df8e52f4b67c68cee8490.png) 

In figure two, the data fields are nested under the data region in the model layout window. As the item number field has been clicked upon, you can see a slight grey around the “Field_0” in the model hierarchy. This is the field that has been selected. 
 
The data fields created are automatically named “Field_[NUMBER]”. To rename your data field, click the tree node representing which field you wish to rename and follow the same steps you took to rename your data region. 