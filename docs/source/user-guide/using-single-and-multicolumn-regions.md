# Using Single and Multi-Column Regions

**Applies To:** 

- ReportMiner Enterprise and Express (all versions) 
- Centerprise Enterprise and Express (all versions) 

## Overview 

A data region is a block of data that makes up the body of your source report within a Report Model. Setting up a data region is one of the first steps to mining data. Data regions are the backbone of Report Models — they tell ReportMiner where to extract data from the file. Once you create a data region, you can add fields to it (data fields specify exactly which data you wish to extract.) Should you export your report document to Excel, these are the values that will be listed in their own cells. For more on data fields, see the data fields article. 

Data regions are highlighted once selected. In the Default mode, they are gray. There are four main types of data regions: standard regions, header regions, footer regions, and append regions.

 

## Pattern Matching

The program makes it possible to quickly create data regions using the pattern matching feature. A data region is not complete without matching a pattern to show which data needs to be included in that region. The orange line with blue dots above the report document is called the pattern matching line. Characters can be typed in here to select lines that either contain the same character (such as a specific letter or number), the same kind of character (see below), or the same phrase.  

![](using-single-and-multicolumn-regions-images/1340c093560bd31d0ce7495ed875aed9b5fcd8875680ddb597ae3a53d294c490.png) 

See the Properties section of this document for a breakdown of the pattern matching properties menu. 

There is a line of buttons for pattern matching above the report model. These can be used by clicking on the pattern matching line and then selecting the relevant button above the area on your report that it applies to. 
![](using-single-and-multicolumn-regions-images/ff1ac836693059fc755dc37b62285578fa8527ce7ac371207b6c1a225d2e5b29.png) 

They are as follows: 

![](using-single-and-multicolumn-regions-images/dc8337959f17d8ab6ddbe71669705d2189b85e83cae4f8a0f707586e4996381a.png) is the “Match any alphabet” button. This button stands for any letter within your document. 

![](using-single-and-multicolumn-regions-images/36f6fca2ff0162c2e74f5a3271bbdc2dfb6be6fce481aae463dae3f10e496194.png) is the “Match any digit” button. Similar to the “Ã” button, this is used to represent any number. For instance, if you were trying to select lines that contained phone numbers, you would type, “ÑÑÑ-ÑÑÑ-ÑÑÑÑ” into the pattern matching line (without the quotation marks.) 

![](using-single-and-multicolumn-regions-images/598cd26ee172bc4247c8974bc5382b3e544d507bb93f69894a778c129ef6e3e8.png) is the “Match any alphabet or digit” button. It can represent numbers and/or letters. 

![](using-single-and-multicolumn-regions-images/1ddf4f9e030b6c7b1f9f3908193478d7b3770d340b715fe22df27a555c58a3a7.png) is the “Match any non-blank character” button. This is used for characters that fit into the previously mentioned categories, but also aren’t tabs, spaces, or any other “blank” character. 

![](using-single-and-multicolumn-regions-images/a200d7601c3c4e5a7efc48202c9281c69a1c4efd0d106540bfe94f3e132f1254.png) is the “Match any blank character” button. This can be used to stand for any character that is "invisible," such as a tab, return/enter, or a space. 

## Example 

In the report model below, the user needs to create a data region to extract all lines of data underneath “Book Title.” There are many ways to go about this: we’ll cover two in this example. In this example, the data region has already been created – go here(insert link or page number) to see how to create a data region from scratch. 

Each line has a date, and in each date there is a “/” character.  

By typing the “/” above where it appears in the lines, the program knows to select every line that has that character in that spot. 

Another way to select all the necessary information would be to use the “Ã“ button, as shown below. 

![](using-single-and-multicolumn-regions-images/13d93edbbb4a75c354fbb6c8d932408d34b50298bef63ed7d7326c687717906f.png) 

As all of the names begin at the same position in the column, the “Ã“ button can be used to match any alphabetical character. All of our data is selected, and our data region has been created. 

## Data Region Properties 

To configure the properties of a data region, right-click it and select Region Properties... from the context menu. The following properties are available: 

![](using-single-and-multicolumn-regions-images/date.png) 

Region Name is the name of your data region. Should you not change the name, it defaults to "Data." We recommend changing your data region's name to avoid confusion. 

Region Details let you further customize your data region. Within the Region Details menu are: 

![](using-single-and-multicolumn-regions-images/238e49becb5f666367ea5410225e9b29faddaab44d1ea71d629a3366e6e4ebde.png) 

Region End Type changes how ReportMiner defines where your region ends. 

- Line Count ends your region after a specified number of lines. 
- Blank Line ends your region every time there’s a blank line. 
- Last Field ends your data region at the last data field within your data region. 
- Another Region Starts ends your data region where another begins. This is used for variable-length data regions and automatically ends your data region when another one starts. 

There is also a Region Properties panel directly above the report model.  

![](using-single-and-multicolumn-regions-images/4c11b01b993dfab3635a9f70ceca0729dd89e05dcbe5f3b0e4d95452a577e56d.png) 

Line Count is used to adjust how many lines long a data region is when in Line Count mode. 

Pattern Count adjusts the number of patterns that ReportMiner matches on your data region. This is helpful if more than one pattern can be used to identify the beginning of your data region. Up to five patterns can be specified at a time. 

Single or Multi-Column is an option that allows ReportMiner to scan multiple columns of a document, such as if a report is formatted in three columns (newspaper-style).  

If the "…" is clicked, the following properties window opens: 

![](using-single-and-multicolumn-regions-images/aad8387cb17ebf3b7d4ae10a386e1ca5a28b600e362a9905315e6b8382b48c51.png) 

Starting Points indicate where your columns begin. To find what the coordinates of your data region are, click on the part of the document that you wish to start your data region. As shown below, the bottom right corner of the report model editor shows the coordinates for that point. 

Automatically Calculate Columns is also within the Multi-Column Region Properties. This finds where the columns begin and end without needing a set of starting points. 

![](using-single-and-multicolumn-regions-images/a5093005881744e2ad892ad0b4401c99dbc047f7ccf853fc70bb3c729a5ecf9e.png) 

Delete Region lets you delete a data region. 

Add Data Region lets you add a data region. 

Append Data Region is a region that you can add as part of a report that would be otherwise left out of the data region. For instance, in the report below, the group totals aren’t captured in the main data region. 

![](using-single-and-multicolumn-regions-images/d3deb3276558d677c0115dd294e1a1b51440c361e8ab8276655efa806c384d0b.png) 

In order to ensure that the totals are included, an append region needs to be added. 

![](using-single-and-multicolumn-regions-images/309f13d179e6e4f2e0b7a8ee8f2dc100157f6e6b263ab2688104e86ba48ff35c.png) 

Now, the totals will be present in the exported report. 

## Adding a Data Region 

To add a data region to your report model, right-click on the Record node and select, "Add Data Region." 

![](using-single-and-multicolumn-regions-images/5a044e60ec1790c2edf07f12d0fa38e319a77119a1bd905ba0f0b5a93a0bb285.png) 

In order for ReportMiner to select a specific area to act as a data region, a pattern needs to be applied. Type the applicable phrase or select the correct "match any" button. For more on pattern matching, click here. 

![](using-single-and-multicolumn-regions-images/8e6a07324119eec2144b1672b633bf8a02f63cc93134b7aee59c38fd515efe49.png) 

Your data region is now complete and should be highlighted. 

![](using-single-and-multicolumn-regions-images/402f4bb9c45838acd7d1f973051f98808357ccc970478e7f586dc9cca821b5b5.png) 

### Adding a Header Region 

Some files, such as medical reports and order receipts, contain headers. This text area repeats at the top of every page in the report, and can be extracted as a different region.

The title, “PATIENT INFORMATION,” the hospital’s name, and the hospital’s address. Like an append region, this might not be captured in a regular data region. Therefore, a header data region is needed. 

To create a header region, right click on the record node and select, "Add Header Region." 

![](using-single-and-multicolumn-regions-images/07c88c850dc5315177a6ecbbc0e0d9b949613901ef78200ddae7a4e61087b197.png) 

Since our header repeats, the easiest way to select the header region is to type a word or phrase as your method of pattern matching. As every page on this report starts with a header that says, "PATIENT INFORMATION," you can use that phrase to select the first line of the header in the pattern matching line. 

If your headers are not perfectly aligned on your report, be sure to click the "Floating Pattern" box. 

![](using-single-and-multicolumn-regions-images/848ba75f5d45807ada0bce1b2c04cc5a859d1d389c54ec329e9d1638145be5d4.png) 

To adjust how many lines tall your header region is, adjust the line count field. 

![](using-single-and-multicolumn-regions-images/15e1942c904ef1e4d5399413a10bc81ee5bac1a2c15a453e4f1b7e1fcc9e2a67.png) 

Your header region is now complete. 

To add a footer data region, right click on the record node and select, "Add Footer Region." 

![](using-single-and-multicolumn-regions-images/793119608fd9266e746137d16f40ffd2515a0503ac817a1eda79bb2febcc08a3.png) 

As the footer repeats, the easiest way to select the footer region is to type a word or phrase as your method of pattern matching. 

![](using-single-and-multicolumn-regions-images/05512c3463b94016a0b3459b506661f7a7a9781d6229a19ca84eba7911e940c1.png) 

As with the header region, adjust your footer region's line height as needed in the "Line Count" field. Your footer region is now complete. 

Some reports have data that isn’t captured in an existing data region, but need to be included with each record in the exported data. In the sheet below, the library name isn’t a part of the captured data. 

![](using-single-and-multicolumn-regions-images/0561f298efc19346ac32e0880c77a4bbcd171928628383d3f204e649431176e4.png) 

In order to capture this data, select your highest record node and right-click. Select “Add Append Region…” from the drop-down menu. 

![](using-single-and-multicolumn-regions-images/5e20406a1e455a1a38b5d5cadcbe0ed6f2b65fa7afa0232c2e87f981723f7159.png) 

Once your append data region is added, you can then add your data region via pattern-matching or the auto-create function as you would any data region. Once a data field has also been added, we see that the data is now included with every record. 

![](using-single-and-multicolumn-regions-images/1c448a9efc7e8d9066adfe0f30f9da2a7bc73115324ff05985bbc48fc43563c2.png) 


## Creating Multi-Column Regions 

Astera offers the “Multi-Column” data region for data that is formatted in multiple columns.

To create a multi-column data region, right click on your record node and select “Add data region…” as you would for a regular region. 

Now, check the “Multi-Column” box. 

![](using-single-and-multicolumn-regions-images/2016-08-19_1607.png)

Another bar will appear under the pattern-matching bar. This is for setting column boundaries. Click on the second bar above where your first column starts. A black line should appear. Make sure that this is flush with the left side of your characters. Repeat for each column start point. If your line is in the wrong place, click on the line to delete it. 

![](using-single-and-multicolumn-regions-images/2016-08-19_161022.png)

You can also adjust the number of columns and column margin by clicking on the "..." . Next to the Multi-column option.

![](using-single-and-multicolumn-regions-images/2016-08-19_16202222.png)

From here, pattern-match as normal to create your data region. 

![](using-single-and-multicolumn-regions-images/2016-08-19_16314444.png)

## Attachments

- [Patient Information3.pdf](using-single-and-multicolumn-regions-images/PatientInformation3.pdf)
- [Library Sheet.pdf](using-single-and-multicolumn-regions-images/LibrarySheet.pdf)
- [sampleorders.txt](using-single-and-multicolumn-regions-images/sampleorders.txt)
- [multicolumn.pdf](using-single-and-multicolumn-regions-images/multicolumn.pdf)