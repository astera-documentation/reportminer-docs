# Creating a Report Model

**Applies To:** 

- Centerprise 
- ReportMiner 

## Overview

### Report Model

A report model is a file created used to define where regions of specific text can be found in the report file. It contains the extraction logic as well as settings to control how data should be exported from the related report file. This model establishes what needs to be extracted from the report file by using specified patterns. This model can be reused and applied to multiple files.  

A report model consists of 3 main components: 

- Data Regions  
- Data fields belonging to those regions.  
- Patterns to define the data regions 

  

### Data Regions 

A data region is an area of data captured within your report. This can encompass any number of lines within your report. There are different types of data regions. These can include data region, header region, footer region, and append region.  

- **Data Region**: This is the most common type of region. This is the area specified by the pattern. 
- **Header**: This is the area of data located at the very top of the page. You would add this region if you are trying to extract header information that may repeat on each page. 
- **Footer**: This is the block of data located at the very bottom of the report.  
- **Append**: This type of region can be used to append additional data to your model. (See Append Regions for more details).  

![](creating-a-report-model-images/e9a0f04f2ae60ce788d1c97d50b1d4a907d59bce97f2a716c2d46d2b2c984c7e.png) 

### Data Fields

A field is an area of data inside a region. This area is the data that needs to be extracted. An example of these data fields within a region can be seen in the below images. For example: Item, Quantity, Description, Item Code, Price, and Total. These are all the fields that will be extracted. In the image below, the fields have been selected (highlighted) and added to the model. 

![](creating-a-report-model-images/6df43f42f2d4f0d8928a63b018299a9ea70e485c60b6325d5e1281dc2746c7d6.png) 

### Patterns 

A pattern identifies the records that you need to extract from the report document. ReportMiner looks for the specified pattern and will capture each line of text that matches this pattern. It is important to be specific with your pattern, to ensure you are capturing the correct lines and only these lines. 

 ![](creating-a-report-model-images/fdec05ed2a74d1adb6ec00820488e8861f7085056504fb8c217656d214962f03.png) 


## Example 

To create a new report model, go to File -> New and select Report Model. 

![](creating-a-report-model-images/47e2e19c7dcc678e143b7d13eef7c2527d69d987b642d82321914750ce5bf577.png) 

The “Report Options” window will pop up. This is where you will select the file you will be extracting from. Select your file and click OK. 

![](creating-a-report-model-images/28d256f83bb00a87cd17a14cf08bb912594597209cee4d17654617160f215ec7.png)  

Your report will open in the report editor window. The next step is to add your data regions. 

Go to the Model Layout and right click on the “Record” node and select “Add Data Region”. 

![](creating-a-report-model-images/e4d4b72ca495722b59d7f15065779ac300016404f42a52b8f34bbb1f9eea6ef4.png)  

Once you add your first data region, the pattern box will appear. This is where you will type in your pattern that will specify your data region. In this example, you can use the "match any digit" symbol. Once you specify the pattern, this area is highlighted.   

![](creating-a-report-model-images/039a6d5dfc0420f6505f9970ba67daabc75fff9fc0ee667029b9ec6bc3a6cd46.png) 

Now you can go in and add your data fields. Highlight the data field area, right click and select “Add Data Field”. These are highlighted once you add each field.  

![](creating-a-report-model-images/b2093193f232219869152fb5f5df3fc26e9d597968f3533eeb3086c9281fe735.png) 

Model Layout: 

![](creating-a-report-model-images/a9a4942bee6265b00a3003014806481864f3e0705b08dd7e6228b408f847e9ac.png) 

This is what your model layout looks like once you add your data region and fields. 

## Best Practices 

- Make sure to change your data region and field names to something relevant. Names cannot contain spaces or special characters. 
- Click the Magnify glass “Preview” button ![](creating-a-report-model-images/a7e9ddcf3ede73eda9f832b73cb3a6dc33e78b2f9f71b1c0a7c8a4b129ffb338.png) to preview your data and make sure everything is extracting correctly. Once you do this, you are ready to export.