# Using Floating Fields 



**Applies To:**     

Centerprise/ReportMiner    

## Overview

A floating field pertains to data points that can appear in various locations throughout your report. The Floating Pattern option within the Report Model component will allow you to capture each data field no matter the location. With this option, you must first specify a pattern. Once the “Floating pattern” option is selected, it will capture all the lines that contain the pattern specified.  

## Pattern Properties 

The Floating pattern is located under the “Pattern Properties” above your report. 

**Float Fields:** 

The “Float Fields” option will automatically appear to the right of the “Floating Pattern” option once selected. The “Float Fields” will ensure that the spacing also floats  and is based off the line used to capture the first field. This is selected by default but can be unchecked if you'd like the field position to be fixed. 

 ![](using-floating-fields-images/blobid0.png)

## Example

- Select File > New > Report model 
- Select the attached file. (VaryingFieldLocation.text)
- Add your data field by right clicking on the "Record" node in the model layout and selecting "add data region". 
- Type in the floating pattern in the pattern box. In this case "CONTACT" 
- Add all of the data fields by highlighting each field area and right clicking then select "add data field" Make sure to also name each field. 
- Select the “Floating Pattern” Option 
- Preview your data.  

![](using-floating-fields-images/blobid1.png)