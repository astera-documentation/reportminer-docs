# PDF Scaling Factor

**Applies To:**  

- Centerprise
- ReportMiner

## Overview

The Scaling Factor applies to your PDF's spacing value. This is located within the report options. The scaling factor depends on the PDF writer that was used to create the file. In many cases PDFs contain different fonts, headers, and footers. This is why the scaling can vary considerably between PDFs.These variations are converted to text and are translated to the software's standard. This includes equal size font for each character and will include equal space to allow for proportionate spacing. In many cases these font and header variations used by the PDF writer causes the scaling to be off when converting. If this is the case, you can fix this by changing the scaling factor option. 

## Example

Go to File > New  > Report Model, select the attached PDF and click OK. 

![](pdf-scaling-factor-images/blobid0.png) 

 

Here you will see the scaling of the data is not lining up correctly. 

Click on the "Report Options" button   ![](pdf-scaling-factor-images/blobid1.png) 

The "Report Options" window will appear. This is where you will adjust the scaling factor option. 

![](pdf-scaling-factor-images/blobid2.png) 

Input a new "Scaling Factor" value. Select a number between 0 and 9 

By default, the scaling factor is always set to 0. When you are experiencing this scaling issue, you must adjust the scaling factor to a new value. 

In this example, the scaling factor is adjusted to 3.9. This PDF is now lining up. See image below: 

![](pdf-scaling-factor-images/blobid3.png)

## Attachments

- [Sample3.pdf](pdf-scaling-factor-images/Sample3.pdf)

## Related Sections

- [Data Regions](using-single-and-multicolumn-regions)
- [Data Fields](data-fields)  
- [Report Options](report-options)
- [Data Export](data-exporting)