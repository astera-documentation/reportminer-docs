# Microsoft Word and Rich Text Format Support

## Microsoft Word (.doc and .docx extension)  

The .doc file extension was proprietary to Microsoft and is still one of the most popular file formats existing today. Software from other types of word processing products had trouble reading files with a .doc extension. As a result, Microsoft created a newer file format called .docx extension. This signifies the Office Open XML international standard for Office documents. .Docx is supported by a growing number of applications from Microsoft and Apple, as well as open-source word processing programs and other vendors. 

## Rich Text Format (.rtf extension) 

The .rtf file format was developed by Microsoft Corporation for cross-platform document interchange with Microsoft products.  

Most word processing software supports RTF format importing/exporting/editing, etc. The RTF Specification uses the ANSI file format. RTF supports generic font family names and also supports inclusion of image file formats such as JPEG and PNG. 

ReportMiner 7 now includes support for Microsoft Word and RTF formats: enjoy efficient and easy information extraction from more source files than ever before. Now you can process invoices, purchase orders, receipts, forms and other Word/RTF-formatted files with ReportMiner 7. 

### The screenshots below illustrate .docx extension support in ReportMiner. 

Select File > New > Report Model and choose your source document.  

![](microsoft-word-and-rich-text-format-support-images/1.png) 

As seen in the screenshot below, the .docx file opens and is ready for processing. 

![](microsoft-word-and-rich-text-format-support-images/2.png) 

You can now continue to create your report model. 

![](microsoft-word-and-rich-text-format-support-images/3.png)